# README

Folders of Python (QGIS or ArcGIS) scripts. 
Includes a short description of each folder below.

## Folders

### rm6388GIS
Graduate course by David Verbyla. The official website for the course
is [here](http://nrm.salrm.uaf.edu/~dverbyla/nrm638/).
Youtube videos to follow along can be found 
[here](https://www.youtube.com/user/dlverbyla)

### scipy2013_geospatial
Scipy2013 tutorial by Kelsey Jordhal titled
'Using geospatial data with python'.
Link, including youtube videos, is [here](http://conference.scipy.org/scipy2013/tutorial_detail.php?id=110)
