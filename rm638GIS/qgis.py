from osgeo import ogr

def get_shp_driver( ):
	"""
	Instantiate and return an ESRI driver.
	"""
	return ogr.GetDriverByName('ESRI Shapefile')
	
def open_shp_file( filename, mode = 0 ):
	"""
	Open shp filename and return data source object.
	mode indicates whether it is: read-only, write, etc.
	mode of 0 is read-only.
	"""
	driver = get_shp_driver( )
	return driver.Open( filename, mode )

# data_src = open_shp_file( 'test_points.shp' )	
# layer = data_src.GetLayer()
# print layer.GetFeatureCount() # For points
# print layer.GetExtent()
# print layer.GetGeomType() 
# returns integer: 1 for points, 3 for polygons, etc
	
if __name__ == '__main__':
	print 'This is the end'